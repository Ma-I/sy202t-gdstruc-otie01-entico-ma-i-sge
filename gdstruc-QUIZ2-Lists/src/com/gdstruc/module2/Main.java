package com.gdstruc.module2;

public class Main {

    public static void main(String[] args) {

        Player asuna = new Player(45, "Asuna",234);
        Player bacon = new Player (234, "Bacon", 1);
        Player hpD = new Player (34, "HP Deskjet", 89);
        Player heath = new Player (432, "Heathcliff", 76);
        Player nan = new Player(2, "Nan", 1232); // Not going to be added to check ThisContains false condition

        PlayerLinkedList players = new PlayerLinkedList();
        players.addToFront(asuna);
        players.addToFront(bacon);
        players.addToFront(hpD);
        players.addToFront(heath);

        players.printList();
        players.IndexOf();
        players.ThisContains(heath);

        System.out.println();

        players.removeFirstItem();
        players.printList();
        players.IndexOf();
        players.ThisContains(nan);
    }
}
