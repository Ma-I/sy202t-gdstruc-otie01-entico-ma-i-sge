package com.gdstruc.trees;

public class Main {

    public static void main(String[] args) {
        Tree tree = new Tree();

        tree.insert(90);
        tree.insert(564);
        tree.insert(-69);
        tree.insert(12);
        tree.insert(768);
        tree.insert(78);
        tree.insert(-100);

        //tree.traverseInOrder();
        tree.reverseInOrder();
        System.out.println("=========");
        tree.getMin();
        System.out.println("=========");
        tree.getMax();
    }
}
