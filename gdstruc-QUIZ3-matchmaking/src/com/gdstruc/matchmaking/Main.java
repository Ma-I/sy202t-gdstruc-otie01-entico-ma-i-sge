package com.gdstruc.matchmaking;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Every turn, x players will queue for matchmaking (x = rand () 1 to 7). Pressing enter ends the turn.
        // A game can be started when at least 5 players are in the queue.
        // When a game starts, pop the first 5 players from the queue.
        // The program terminates when 10 games have been successfully made.

        Scanner playerInput = new Scanner(System.in);
        Random dice = new Random();

        PlayQueue newQueue = new PlayQueue(10);

        Player ella = new Player("Glass Maker",99);
        Player mulan = new Player("Dragon Warrior", 54);
        Player belle = new Player("Beast Tamer", 87);
        Player aurora = new Player( "Magic Caster", 676);
        Player ariel = new Player("Ocean Master", 34);

        int game = 1;
        while (game < 11) {
            int randLoop = 1 + dice.nextInt(7);
            for (int loop = 0; loop < randLoop; loop++) {
                int playerDice = 1 + dice.nextInt(5);

                switch (playerDice) {
                    case 1 -> newQueue.enqueue(ella);
                    case 2 -> newQueue.enqueue(mulan);
                    case 3 -> newQueue.enqueue(belle);
                    case 4 -> newQueue.enqueue(aurora);
                    case 5 -> newQueue.enqueue(ariel);
                }
            }

            // Game UI
            System.out.println("=================");
            System.out.println("GAME #: " + game);
            System.out.println("Number of Players: " + newQueue.size());
            System.out.println("List of Players:");
            newQueue.printQueue();
            System.out.println("-------------------");

            if (newQueue.size() >= 5) {
                System.out.println("Matchmaking IN PROGRESS...");

                int match = 1;
                if(newQueue.size() % 2 == 0) {
                    while (newQueue.size() > 0) {
                        System.out.println("MATCH #: " + match);
                        System.out.print(newQueue.peek() + " vs ");
                        newQueue.remove();

                        System.out.print(newQueue.peek());
                        newQueue.remove();
                        System.out.println("");

                        ++match;
                    }
                }
                if(newQueue.size() % 2 == 1) {
                    while (newQueue.size() > 1) {
                        System.out.println("MATCH #: " + match);
                        System.out.print(newQueue.peek() + " vs ");
                        newQueue.remove();

                        System.out.print(newQueue.peek());
                        newQueue.remove();
                        System.out.println("");

                        ++match;
                    }
                    System.out.println("-------------------");
                    System.out.println(newQueue.peek() + " is left in the queue...");
                }

                System.out.println("Matchmaking has been completed...");
                //adds to game count
                game++;

            } else {
                System.out.println("Not enough players have been queued, GAME ABORTED...");
            }
        }
        System.out.println("-------------------");
        System.out.println("10 Games have already been made - GAME OVER -");
    }

}
