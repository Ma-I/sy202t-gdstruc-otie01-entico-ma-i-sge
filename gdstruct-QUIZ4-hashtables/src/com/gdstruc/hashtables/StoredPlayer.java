package com.gdstruc.hashtables;

public class StoredPlayer {
    public String key;
    public Player value;

    public StoredPlayer(String key, Player value) {
        this.key = key;
        this.value = value;
    }

    // I put this here because the memory address was showing instead of the tags
    @Override
    public String toString() {
        return "StoredPlayer{" +
                "key='" + key + '\'' +
                ", value=" + value +
                '}';
    }
}
