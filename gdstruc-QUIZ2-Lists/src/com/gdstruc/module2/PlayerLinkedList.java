package com.gdstruc.module2;

public class PlayerLinkedList {
    private PlayerNode head;

    public void addToFront (Player player){
        PlayerNode nodeFront = new PlayerNode(player);
        nodeFront.setNextPlayer(head);
        head = nodeFront;
    }

    public void printList () {
        PlayerNode current = head;
        System.out.print("HEAD -> ");

        while (current != null){
            System.out.print(current.getPlayer() + " -> ");
            current = current.getNextPlayer();
        }
        System.out.println("NULL");
    }

    public void ThisContains(Player player) {
        PlayerNode search = new PlayerNode(player);
        PlayerNode current = head;

        System.out.print("Searching for player... ");
        while (current != null) {

            if (current.getPlayer() == search.getPlayer()) {
                System.out.println("Player FOUND");
                break;
            }
            current = current.getNextPlayer();
        }

        if (current == null) {
            System.out.println("Player does not exist...");
        }
    }

    public void IndexOf() {
        int cnt = 0;
        PlayerNode current = head;

        while (current != null){
            current = current.getNextPlayer();
            cnt++;
        }
        System.out.println("Number of players present: " + cnt);
    }

    public void removeFirstItem(){
        PlayerNode newHead = head;
        newHead = newHead.getNextPlayer();
        head = newHead;
    }

}