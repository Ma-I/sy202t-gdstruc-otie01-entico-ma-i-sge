package com.gdstruc.midterms;

import java.util.Objects;

public class PlayerCards {
    private String cardName;
    private String fighterClass;
    private int lvl;

    public PlayerCards(String cardName, String fighterClass, int lvl) {
        this.cardName = cardName;
        this.fighterClass = fighterClass;
        this.lvl = lvl;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerCards that = (PlayerCards) o;
        return lvl == that.lvl && Objects.equals(cardName, that.cardName) && Objects.equals(fighterClass, that.fighterClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardName, fighterClass, lvl);
    }

    @Override
    public String toString() {
        return "PlayerCards{" +
                "cardName='" + cardName + '\'' +
                ", fighterClass='" + fighterClass + '\'' +
                ", lvl=" + lvl +
                '}';
    }
}
