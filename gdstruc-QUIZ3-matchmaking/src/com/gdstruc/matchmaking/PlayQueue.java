package com.gdstruc.matchmaking;

import java.util.NoSuchElementException;
import java.util.Queue;

public class PlayQueue {
    private Player[] queue;
    private int front;
    private int back;

    public PlayQueue(int capacity) {
        queue = new Player[capacity];
    }

    public void enqueue (Player player) {
        if (back == queue.length){
            Player[] newQueue = new Player[queue.length * 2];
            System.arraycopy(queue, 0, newQueue, 0, queue.length);
            queue = newQueue;
        }
        queue[back] = player;
        back++;
    }

    public int size (){
        return back-front;
    }

    public Player remove() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        Player removedPlayer = queue[front];
        queue[front] = null;
        front++;

        if (size() == 0) {
            front = 0;
            back = 0;
        }
        return removedPlayer;
    }

    public Player peek() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        return queue[front];
    }

    public void printQueue() {
        for (int i = front; i < back; i++){
            System.out.println(queue[i]);
        }
    }
}
