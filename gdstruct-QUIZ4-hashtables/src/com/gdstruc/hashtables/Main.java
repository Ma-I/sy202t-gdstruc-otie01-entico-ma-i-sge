package com.gdstruc.hashtables;

public class Main {

    public static void main(String[] args) {

        Hashtable playerRoster = new Hashtable();

        Player ella = new Player(23423, "Glass Maker",99);
        Player mulan = new Player(453, "Dragon Warrior", 54);
        Player belle = new Player(654,  "Beast Tamer", 87);
        Player aurora = new Player(42534, "Magic Caster", 676);
        Player ariel = new Player(55657, "Ocean Master", 34);

        playerRoster.put(ella.getUserName(), ella);
        playerRoster.put(mulan.getUserName(), mulan);
        playerRoster.put(belle.getUserName(), belle);
        playerRoster.put(aurora.getUserName(), aurora);
        playerRoster.put(ariel.getUserName(), ariel);

        playerRoster.printHashTable();
        playerRoster.get2(ella.getUserName());
        playerRoster.remove(mulan.getUserName());
        playerRoster.printHashTable();
        playerRoster.get2(mulan.getUserName());
    }
}
