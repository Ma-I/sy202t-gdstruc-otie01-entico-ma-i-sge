package com.gdstruc.matchmaking;

import java.util.Objects;

public class Player {
    private String name;
    private int lvl;

    public Player(String name, int lvl) {
        this.name = name;
        this.lvl = lvl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", lvl=" + lvl +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return lvl == player.lvl && Objects.equals(name, player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lvl);
    }
}
