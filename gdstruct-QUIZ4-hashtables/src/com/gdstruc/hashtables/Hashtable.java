package com.gdstruc.hashtables;


import java.awt.desktop.SystemEventListener;

public class Hashtable {
    private StoredPlayer[] hashtable;

    public Hashtable() {
        hashtable = new StoredPlayer[10];
    }

    private int hashKey(String key) {
        return key.length() % hashtable.length;
    }

    private boolean isOccupied(int index) {
        return hashtable[index] != null;
    }

    public void put(String key, Player value) {
        int hashedKey = hashKey(key);

        if (isOccupied(hashedKey)) {
            int stoppingIndex = hashedKey;

            if (hashedKey == hashtable.length - 1) {
                hashedKey = 0;
            } else {
                hashedKey++;
            }
            while (isOccupied(hashedKey) && hashedKey != stoppingIndex) {
                hashedKey = (hashedKey + 1) % hashtable.length;
            }
        }
        if (isOccupied(hashedKey)) {
            System.out.println("Sorry, there is already an element at position " + hashedKey);
        } else {
            hashtable[hashedKey] = new StoredPlayer(key, value);
        }
    }

    // I made a get2 function because this was returning a blank space instead
/*    public Player get(String key) {
        int hashedKey = hashKey(key);

        if (hashedKey == -1)
        {
            return null;
        }
        return hashtable[hashedKey].value;
    }*/

    public void get2(String key) {
        int hashedKey = hashKey(key);
        System.out.println("");
        System.out.println("==============================");
        System.out.println("Searching for player... ");

        if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key)) {
            System.out.println("Player found: " + hashtable[hashedKey]);
            System.out.println("==============================");
            System.out.println("");
        } else if (hashtable[hashedKey] == null) {
            System.out.println("That player does not exist... ");
            System.out.println("==============================");
            System.out.println("");
        }
    }

    private int findKey(String key) {
        int hashedKey = hashKey(key);

        if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key)) {
            return hashedKey;
        }

        int stoppingIndex = hashedKey;

        if (hashedKey == hashtable.length - 1) {
            hashedKey = 0;
        } else {
            hashedKey++;
        }
        while (hashedKey != stoppingIndex && hashtable[hashedKey] != null && !hashtable[hashedKey].key.equals(key)) {
            hashedKey = (hashedKey + 1) % hashtable.length;
        }
        if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key)) {
            return hashedKey;
        }
        return -1;
    }

    public void printHashTable() {
        System.out.println("");
        System.out.println("************");
        System.out.println("PLAYER ROSTER: ");

        for (int i = 0; i < hashtable.length; i++) {
            System.out.println("Element " + i + " " + hashtable[i]);
        }
        System.out.println("************");
        System.out.println("");
    }

    public void remove(String key) {
        int hashedKey = hashKey(key);

            if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key)) {
                hashtable[hashedKey] = null;
            }

        System.out.println("");
        System.out.println("========================================");
        System.out.println("Player has been successfully removed...");
        System.out.println("========================================");
        System.out.println("");

    }
}

