package com.gdstruc.midterms;

import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedStack {

    private LinkedList<PlayerCards> stack;

    public LinkedStack()
    {
        stack = new LinkedList<PlayerCards>();
    }

    public void push(PlayerCards card) {
        stack.push(card);
    }

    public PlayerCards pop() {
        return stack.pop();
    }

    public PlayerCards peek() {
        return stack.peek();
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }

    public void printStack() {
        ListIterator<PlayerCards> iterator = stack.listIterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        if(isEmpty()) {
            System.out.println("There are no cards...");
        }
    }

    public int countStack() {
        ListIterator<PlayerCards> iterator = stack.listIterator();
        int cards = 0;

        while (iterator.hasNext()) {
            cards++;
            iterator.next();
        }
        return cards;
    }
}
