package com.gdstruc.trees;

public class Tree {
    private Node root;

    public void insert(int value) {
        if (root == null) {
            root = new Node(value);
        } else {
            root.insert(value);
        }
    }

    public void traverseInOrder() {
        if (root != null) {
            root.traverseInOrder();
        }
    }

    public void reverseInOrder() {
        if (root != null) {
            root.reverseInOrder();
        }
    }

    public void getMin() {
/*        if (root != null){
            root.getMin();
        }*/
        root.getMin();
        //System.out.println(root.getMin());

    }

    public void getMax() {
/*        if (root != null){
            root.getMax();
        }*/
        root.getMax();

    }

    public Node get(int value) {
        if (root != null) {
            return root.get(value);
        }
        return null;
    }
}
