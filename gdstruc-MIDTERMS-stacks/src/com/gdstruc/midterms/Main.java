package com.gdstruc.midterms;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        LinkedStack playStack = new LinkedStack();
        LinkedStack playerDeck = new LinkedStack();
        LinkedStack discardedStack = new LinkedStack();

        PlayerCards ella = new PlayerCards("Cinderella", "Glass Maker",99);
        PlayerCards mulan = new PlayerCards("Mulan", "Dragon Warrior", 54);
        PlayerCards belle = new PlayerCards("Belle",  "Beast Tamer", 87);
        PlayerCards aurora = new PlayerCards("Aurora", "Magic Caster", 676);
        PlayerCards ariel = new PlayerCards("Ariel", "Ocean Master", 34);

        Random diceDupe = new Random();
        int dice;

        // populates the stack
        for (int loop = 0; loop < 30; loop++){
            dice = 1+diceDupe.nextInt(5);

            if (dice == 1){
                playStack.push(ella);
            }
            if (dice == 2){
                playStack.push(mulan);
            }
            if (dice == 3){
                playStack.push(belle);
            }
            if (dice == 4){
                playStack.push(aurora);
            }
            if (dice == 5){
                playStack.push(ariel);
            }
        }

        Scanner playerInput = new Scanner(System.in);

        while (playStack.countStack() > 0) {
            dice = 1+diceDupe.nextInt(3);

            //Player UI
            System.out.println("No. of cards left: " + playStack.countStack());

            System.out.println("No. of cards discarded: " + discardedStack.countStack());

            System.out.println("Cards at hand:");
            playerDeck.printStack();
            System.out.println();

            // Random Turn 1 :: DRAW FROM PLAY PILE
            if (dice == 1) {
                System.out.println("[Please draw 1-5 cards from the pile]");
                System.out.print("How many cards do you want to DRAW from the pile?: ");
                int input = playerInput.nextInt();

                while (input <= 0 || input > 5) {
                    System.out.println("Invalid number, please try again...");
                    System.out.print("How many cards do you want to DISCARD from the pile?: ");
                    input = playerInput.nextInt();
                }

                    // In case there's only single digit cards left
                    if (input > playStack.countStack()) {
                        input = playStack.countStack();
                    }

                    System.out.println("You drew " + input + " cards...");

                    for (int loop = 0; loop < input; loop++) {
                        playerDeck.push(playStack.peek());
                        playStack.pop();
                    }
            }

            // Random Turn 2 :: DISCARD
            else if (dice == 2) {
                if (playerDeck.isEmpty()){
                    System.out.println("Player deck is empty... Re-rolling...");
                }
                else {
                    System.out.println("[Please DISCARD cards from the pile]");
                    System.out.print("How many cards do you want to DISCARD from the pile?: ");
                    int input = playerInput.nextInt();

                    while (input > playerDeck.countStack() || input <= 0) {
                        System.out.println("Invalid number, please try again...");
                        System.out.print("How many cards do you want to DISCARD from the pile?: ");
                        input = playerInput.nextInt();
                    }

                    for (int loop = 0; loop < input; loop++) {
                        discardedStack.push(playerDeck.peek());
                        playerDeck.pop();
                    }
                }
            }

            // Random Turn 3 :: DRAW FROM DISC PILE
            else if (dice == 3) {
                if (discardedStack.isEmpty()){
                    System.out.println("Discarded pile is empty... Re-rolling...");
                }
                else {
                    System.out.println("[Please draw 1-5 cards FROM THE DISCARD pile]");
                    System.out.print("How many cards do you want to DRAW from the pile?: ");
                    int input = playerInput.nextInt();

                    while (input <= 0 || input > 5 || input > discardedStack.countStack()) {
                        System.out.println("The number you entered is invalid, please try again...");
                        System.out.print("How many cards do you want to DRAW from the pile?: ");
                        input = playerInput.nextInt();
                    }

                    for (int loop = 0; loop < input; loop++) {
                        playerDeck.push(discardedStack.peek());
                        discardedStack.pop();
                    }
                }
            }
            System.out.println("------------");
            System.out.println();
        }
        System.out.println("------ OUT OF FRESH CARDS TO DRAW -------");
    }
}
