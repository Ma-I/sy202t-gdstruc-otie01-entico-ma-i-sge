package com.gdstruc.trees;

public class Node {
    private int data;
    private Node leftChild;
    private Node rightChild;

    public int getData() {
        return data;
    }

    public Node(int _data) {
        this.data = _data;
    }

    public void insert(int value) {
        if (value == data) {
            return;
        }

        if (value < data) {

            if (leftChild == null) {
                leftChild = new Node(value);
            } else {
                leftChild.insert(value);
            }

        } else {

            if (rightChild == null) {
                rightChild = new Node(value);
            } else {
                rightChild.insert(value);
            }
        }
    }

    public void traverseInOrder() {
        //System.out.println("Data: " + data);
        if (leftChild != null) {
            leftChild.traverseInOrder();
        }
        System.out.println("Data: " + data);
        if (rightChild != null) {
            rightChild.traverseInOrder();
        }
       //System.out.println("Data: " + data);
    }

    public void reverseInOrder() {
        if (rightChild != null) {
            rightChild.reverseInOrder();
        }
        System.out.println("Data: " + data);
        if (leftChild != null) {
            leftChild.reverseInOrder();
        }
    }

    public Node get(int value) {
        if (value == data) {
            return this;
        }

        if (value < data) {
            if (leftChild != null) {
                return leftChild.get(value);
            }
        } else {
            if (rightChild != null) {
                return rightChild.get(value);
            }
        }
        return null;
    }

    // having problems with reversing this "linked list"
    // tried to go by the logic of - far left == where the smoler numbers end up - far right == where big numbers end up
    public Node getMin() {
        if (leftChild != null) {
            leftChild.getMin();
        }
        System.out.println("Min Number: " + leftChild);
        return leftChild;
    }

/*    public void getMin() {
        if (leftChild != null) {
            leftChild.getMin();
        }
        System.out.println("Min Number: " + leftChild);
    }*/

    public void getMax() {
        if (rightChild != null) {
            rightChild.getMax();
        }
        System.out.println("Max Number: " + rightChild);
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                '}';
    }
}
