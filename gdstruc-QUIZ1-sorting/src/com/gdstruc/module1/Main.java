package com.gdstruc.module1;

public class Main {

    public static void main(String[] args) {

        int [] numbers = new int [10];

        numbers [0] = 100;
        numbers [1] = 24;
        numbers [2] = 77;
        numbers [3] = -9;
        numbers [4] = 2553;
        numbers [5] = 0;
        numbers [6] = 5;
        numbers [7] = 11;
        numbers [8] = 304;
        numbers [9] = 1;

        System.out.print("Unsorted Numbers: ");
        printArray(numbers);
        System.out.println();

        System.out.print("Sorted Numbers: ");
        selectionSort(numbers);
        printArray(numbers);
    }

    private static void bubbleSort (int arr[])
    {
        for (int lastItem = arr.length - 1; lastItem > 0 ; lastItem--)
        {
            for (int i = 0; i < lastItem; i++)
            {
                // descending order :: flip the sign around
                if (arr[i] < arr[i+1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
        }
    }

    private static void selectionSort (int arr[])
    {
        for (int lastItem = arr.length - 1; lastItem > 0 ; lastItem--)
        {
            int minDex = lastItem ;

            for (int i = 0; i < lastItem; i++)
            {
                if (arr[i] < arr[minDex])
                {
                    minDex = i;
                }
            }

            int temp = arr[lastItem];
            arr[lastItem] = arr[minDex];
            arr[minDex] = temp;

        }
    }

   /* // took the same thing and reversed everything here
    private static void selectionSort2 (int arr[])
    {
        for (int lastItem = 0; lastItem < arr.length - 1 ; lastItem++)
        {
            // equating this to 0 was giving me is error where the value 0 was always put first
            // I think his is bc making minDex = 0 makes the loop keep looping back to [0]
            int minDex = lastItem ;

            for (int i = arr.length - 1; i > lastItem; i--)
            {
                if (arr[i] > arr[minDex])
                {
                    minDex = i;
                }
            }

            int temp = arr[lastItem];
            arr[lastItem] = arr[minDex];
            arr[minDex] = temp;

        }
    }*/

    private static void printArray(int arr[])
    {
        for (int j : arr)
        {
            System.out.print(j + ", ");
        }
    }
}
